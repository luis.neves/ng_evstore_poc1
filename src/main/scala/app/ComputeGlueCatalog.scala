package app

import org.apache.spark.sql.SparkSession

object ComputeGlueCatalog {

  val warehousePath: String = "s3://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/glue_cat/"

  //spark-shell
  //--conf spark.sql.extensions=org.apache.iceberg.spark.extensions.IcebergSparkSessionExtensions
  //--conf spark.sql.catalog.bs=org.apache.iceberg.spark.SparkCatalog
  //--conf spark.sql.catalog.bs.warehouse=s3://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/glue_cat/
  //--conf spark.sql.catalog.bs.catalog-impl=org.apache.iceberg.aws.glue.GlueCatalog
  //--conf spark.sql.catalog.bs.io-impl=org.apache.iceberg.aws.s3.S3FileIO

  lazy val sparkSession = {
    SparkSession.builder()
      .appName("test")
      .config("spark.sql.extensions", "org.apache.iceberg.spark.extensions.IcebergSparkSessionExtensions")
      .config("spark.sql.catalog.bs", "org.apache.iceberg.spark.SparkCatalog")
      .config("spark.sql.catalog.bs.warehouse", warehousePath)
      .config("spark.sql.catalog.bs.catalog-impl", "org.apache.iceberg.aws.glue.GlueCatalog")
      .getOrCreate()
  }


  def withSpark(stop: Boolean = false, func: SparkSession => Unit): Unit = {
    try {
      initializeSession()

      func(sparkSession)
    } finally {
      if (stop) {
        sparkSession.stop()
        System.clearProperty("spark.driver.port")
      }
    }
  }

  def initializeSession(): Unit = {
    sparkSession.sparkContext.setLogLevel("INFO")
  }
}
