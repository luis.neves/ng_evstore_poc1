package app

import app.shodan.SourceShodan
import org.apache.spark.internal.Logging
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object AggShodan extends Logging {

  def createEvShodanTable(spark: SparkSession,  outputTablePath: String, shodanTableName: String): Unit = {


    spark.sql(
      s"""
CREATE TABLE $shodanTableName (
       _shodan struct<id: string,module: string, crawler: string, options: struct<hostname: string, referrer: string, scan: string>, ptr: boolean>
       , ev_ts timestamp
       , ev_date date
       , hash  integer
       , asn string
       , transport  string
       , info string
       , port integer
       , version integer
       , isp string
       , os string
       , product string
       , domains array<string>
       , hostnames array<string>
       , bs_hostname string
       , tags array<string>
       , ssl string
       , is_ssl boolean
       , cpe array<string>
       , cpe23 array<string>
       , vulns map<string, struct<cvss: double, references: array<string>, summary: string, verified: boolean>>
       , data string
       , inet_addr string
       , inet_version string
       , http struct<host: string, location: string, redirects: array<struct<host: string, location: string>>>
       , origin string
     )
     USING delta
     PARTITIONED BY (inet_version, ev_date)
     LOCATION '$outputTablePath';
       """)

  }

  def loadEvShodanTable(spark: SparkSession, pathToRawShodanFiles: String, shodanTableName: String): Unit = {

    spark.time(
      spark
        .read
        .schema(SourceShodan.getSchema())
        .json(pathToRawShodanFiles)
        .withColumnRenamed("timestamp", "ev_ts")
        .withColumn("ev_date", to_date(col("ev_ts")))
        .withColumn("inet_str", coalesce(col("ipv6"), col("ip_str")))
        .withColumn("is_ssl", col("ssl").isNotNull)
        .withColumn("bs_hostname", coalesce(col("http.host"), col("_shodan.options.hostname")))
        .dropDuplicates("inet_str", "bs_hostname", "is_ssl", "transport", "port", "ev_date")
        .withColumn("inet_addr", Udf.full_inet_addr(col("inet_str")))
        .withColumn("inet_version", Udf.inet_version(col("inet_str")))
        .withColumn("origin", input_file_name())
        .drop("ip_str", "ipv6", "inet_str")
        .sort("inet_addr")
        .write
        .format("delta")
        .mode("append")
        .saveAsTable(shodanTableName)
    )
  }

}