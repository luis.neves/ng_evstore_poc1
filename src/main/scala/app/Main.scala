package app

import org.apache.spark.internal.Logging

object Main extends Logging {

  def main(args: Array[String]): Unit = {

    ComputeSparkCatalog.initializeSession()

    ComputeSparkCatalog.withSpark(false, spark => {

      val pathToRawShodanFiles ="/Users/luisneves/bs_sources/shodan/sample.json"
      //val pathToRawShodanFiles = "s3://com.bitsighttech.collection.production/shodan/2023/03/{01,02,03,04,05,06,07}/*.json.gz"
      //val pathToRawShodanFiles = "s3://com.bitsighttech.collection.production/shodan/2023/03/01/*.json.gz"
      //val outputTablePath = "s3://com.bitsighttech.tmp.autodelete.ninetyday/deltalake_poc1/sp1_db"
      val outputTablePath = "/Users/luisneves/bs_ng_evstore/delta_shodan"
      val tableName = "d_ev_shodan"

      AggShodan.createEvShodanTable(spark, outputTablePath, tableName)
      AggShodan.loadEvShodanTable(spark, pathToRawShodanFiles, tableName)

      spark.table(tableName).show()
    })
  }
}