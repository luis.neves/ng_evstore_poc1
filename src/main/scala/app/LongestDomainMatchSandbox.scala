package app

import app.ComputeGlueCatalog.sparkSession
import org.apache.commons.lang3.StringUtils
import org.apache.spark.internal.Logging
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import java.time.LocalDate

object LongestDomainMatchSandbox extends Logging {

  val generate_subddomains = udf((domainName: String) => Option(domainName).map(s => generateSubdomains(s).toArray))

  def generateSubdomains(domainName: String): IndexedSeq[(Int, String)] = {
    val parts = StringUtils.split(domainName, ".")
    for (i <- 0 to parts.length - 1) yield (parts.length - i, (parts.drop(i).mkString("", ".", ".")))
  }

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder()
      .appName("domain_matching")
      .config("spark.master", "local[*]")
      .getOrCreate()

    spark.sparkContext.setLogLevel("WARN")

    import spark.implicits._

    Seq(
      (LocalDate.parse("2023-01-01"), "xpto.aaa.com"),
      (LocalDate.parse("2023-03-01"), "foo.aaa.com"),
      (LocalDate.parse("2023-03-01"), "bar.zzz.com"),
      (LocalDate.parse("2023-03-01"), "foo.bar.bbbb.aaa.com"),
      (LocalDate.parse("2023-03-01"), "ccc.bbb.aaa.com")
    ).toDF("ev_date", "hostname")
      .withColumn("host_exp", explode(generate_subddomains(col("hostname"))))
      .withColumn("host_ord", col("host_exp._1"))
      .withColumn("host_label", col("host_exp._2"))
      .drop("host_exp")
      .createOrReplaceTempView("event_data")

    spark.sql("select * from event_data;").show(100, false)

    Seq(
      ("e1", "aaa.com.", LocalDate.parse("2023-02-01"), LocalDate.parse("2023-04-01")),
      ("e2", "bbb.aaa.com.", LocalDate.parse("2023-02-01"), LocalDate.parse("2023-04-01")),
      ("e3", "bbbb.aaa.com.", LocalDate.parse("2023-02-01"), LocalDate.parse("2023-04-01")),
      ("e4", "ccc.bbbb.aaa.com.", LocalDate.parse("2023-02-01"), LocalDate.parse("2023-04-01")),
      ("e4", "ccc.bbb.aaa.com.", LocalDate.parse("2023-02-01"), LocalDate.parse("2023-04-01"))
    ).toDF("entity_id", "domain", "val_from", "val_until")
      .createOrReplaceTempView("resolved_domain")

    spark.sql("select * from resolved_domain;").show(100, false)

    spark.sql(
      """
WITH hosts_cte AS (
SELECT
  *
  , ROW_NUMBER() OVER (PARTITION BY ed.hostname ORDER BY ed.host_ord DESC) AS ix
FROM
  event_data ed, resolved_domain rd
 WHERE
  ed.host_label = rd.domain
  AND ed.ev_date >= rd.val_from
  AND ed.ev_date <= rd.val_until
)
SELECT hostname, entity_id, domain FROM hosts_cte WHERE ix = 1
             """)
      .show(false)
  }
}
