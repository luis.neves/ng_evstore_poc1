package app

import org.apache.spark.sql.SparkSession

object ComputeSparkCatalog {

  lazy val sparkSession = {
    SparkSession.builder()
      .appName("test")
      //.config("spark.hadoop.fs.s3.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")
      .config("spark.hadoop.fs.s3a.path.style.access", "true")
      .config("spark.sql.caseSensitive", true)
      .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
      .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
      .getOrCreate()
  }

  def withSpark(stop: Boolean = false, func: SparkSession => Unit): Unit = {
    try {
      initializeSession()

      func(sparkSession)
    } finally {
      if (stop) {
        sparkSession.stop()
        System.clearProperty("spark.driver.port")
      }
    }
  }

  def initializeSession(): Unit = {
    sparkSession.sparkContext.setLogLevel("INFO")
  }
}
