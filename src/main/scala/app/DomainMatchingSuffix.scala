package app

import java.time.{LocalDate, LocalDateTime}
import org.apache.commons.lang3.StringUtils
import org.apache.spark.internal.Logging
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

object DomainMatchingSuffix extends Logging {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder()
      .appName("domain_matching_suffix")
      .config("spark.hadoop.fs.s3a.path.style.access", "true")
      .getOrCreate()

    val fs = Env.s3Scheme(spark)

    val hostsSchema = StructType(Array(
      StructField("hostname", StringType, true)
    ))

    val edschema = StructType(Array(
      StructField("domain", StringType, true),
      StructField("val_from", DateType, true),
      StructField("val_until", DateType, true),
      StructField("entity_id", StringType, true),
      StructField("tags", StringType, true),
      StructField("xxx", StringType, true)
    ))

    val udPath = s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/unique_domains.txt.gz"

    spark
      .read
      .schema(hostsSchema)
      .csv(udPath)
      .withColumn("ev_date", current_date())
      .withColumn("host_exp", explode(Udf.generate_subddomains(col("hostname"))))
      .withColumn("host_ord", col("host_exp._1"))
      .withColumn("host_label", col("host_exp._2"))
      .drop("host_exp")
      .createOrReplaceTempView("event_data")

    val dt = LocalDate.now;
    val rdPath = f"${fs}%s://com.bitsighttech.pipeline.entitymaps.production/all-time/${dt.getYear}%s/${dt.getMonthValue}%02d/${dt}%s_entities-resolved-domain-all-time.csv.gz"

    spark
      .read
      .schema(edschema)
      .csv(rdPath)
      .createOrReplaceTempView("resolved_domain")

    spark.time(spark.sql(
      """
EXPLAIN WITH hosts_cte AS (
SELECT
  *
  , ROW_NUMBER() OVER (PARTITION BY ed.hostname ORDER BY ed.host_ord DESC) AS ix
FROM
  event_data ed
  INNER JOIN resolved_domain rd ON ( ed.host_label = rd.domain AND ed.ev_date >= rd.val_from AND ed.ev_date <= rd.val_until)
)
SELECT hostname, entity_id, domain FROM hosts_cte WHERE ix = 1
             """)
      .write
      .mode(SaveMode.Overwrite)
      .csv(s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/matched_domains_suffix/"))
  }
}
