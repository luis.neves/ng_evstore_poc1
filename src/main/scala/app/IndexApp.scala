package app

import org.apache.spark.internal.Logging
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.functions.input_file_name

object IndexApp extends Logging {

  def main(args: Array[String]): Unit = {

    ComputeSparkCatalog.initializeSession()

    ComputeSparkCatalog.withSpark(false, spark => {
      spark
        .read
        .parquet("s3://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/glue_cat/default.db/ev_shodan/data/")
        .withColumn("origin", input_file_name())
        .select("inet_addr", "ev_date", "origin")
        .distinct()
        .write
        .mode(SaveMode.Overwrite)
        .parquet("s3://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/index_table")
    })
  }
}
